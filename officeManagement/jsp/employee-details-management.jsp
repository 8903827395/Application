<html>
<body>
<link href='http://localhost:8080/officeManagement/jsp/btn.css' rel='stylesheet' type='text/css'>
<div class="container">
    <a href="http://localhost:8080/officeManagement/jsp/add-employee.jsp" class="btn">
        <svg width="278" height="60">
        <defs>
            <linearGradient id="grad1">
            <stop offset="0%" stop-color="#FF8282"/>
            <stop offset="100%" stop-color="#E178ED" />
            </linearGradient>
        </defs>
        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
        </svg>
        <span>Create Employee</span>
    </a>

    <a href="http://www.google.com" class="btn">
        <svg width="278" height="60">
        <defs>
            <linearGradient id="grad1">
            <stop offset="0%" stop-color="#FF8282"/>
            <stop offset="100%" stop-color="#E178ED" />
            </linearGradient>
        </defs>
        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
        </svg>
        <span>Delete Employee</span>
    </a>

    <a href="http://www.google.com" class="btn">
        <svg width="278" height="60">
        <defs>
            <linearGradient id="grad1">
            <stop offset="0%" stop-color="#FF8282"/>
            <stop offset="100%" stop-color="#E178ED" />
            </linearGradient>
        </defs>
        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
        </svg>
        <span>Edit Employee</span>
    </a>

    <a href="http://www.google.com" class="btn">
        <svg width="278" height="60">
        <defs>
            <linearGradient id="grad1">
            <stop offset="0%" stop-color="#FF8282"/>
            <stop offset="100%" stop-color="#E178ED" />
            </linearGradient>
        </defs>
        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
        </svg>
        <span>Search Employee</span>
    </a>
</div>
</body>
</html>
