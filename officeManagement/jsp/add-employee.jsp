<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("button").click(function(){
        $("#personal").fadeToggle();
        $("#address").fadeToggle();
    });
});
</script>
</head>

<style>
body {
  width: 100%;
  height:100%;
  position:relative;
  overflow:hidden;
  background: 
    linear-gradient(
      rgba(0, 0, 0, 0), 
      rgba(0, 0, 0, 0)
    ),
    url('http://localhost:8080/officeManagement/images/employee.jpg') 50% 50% no-repeat;
   //background: 
  background-blend-mode: luminosity;
  background-size: cover;   
  
}

input[type=text], select {
    width: 25%;
    padding: 12px 10px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    top: 200px;
    right: 500px; 
    width: 135px;
    height: 47px;
    background-color: #00887b;
    color: white;
    padding: 10px 10px;
    margin-top: 15px ;
    margin-bottom: 40px ;
    margin-left: 100px ;
    margin-right: 100px ;
    position: absolute;
    border: none;
    border-radius: 4px;
}

.button {
    width: 10%;
    background-color: #00887b;
    color: white;
    padding: 10px 10px;
    margin-top: 10px ;
    margin-bottom: 20px ;
    margin-left: 80px ;
    border: none;
    border-radius: 4px;
}

input[type=submit]:hover {
    background-color: #45a049;
    cursor:pointer;
}

div {
    border-radius: 5px;
    padding: 50px;
}

</style>



<body>

    <div>
        <form action="https://www.google.co.in">
        <div id="personal">
            <label for="name">Name</label><br>
            <input type="text" required="" pattern="^[a-zA-Z ]{1,20}$" value="" name="name" id="name" list="name_datalist" placeholder="Enter Your Name "><br>

            <label for="mobile">Mobile Number</label><br>
            <input type="text" required="" pattern="[789]{1}\d{9}" value="" name="mobile" id="mobile" list="mobile_datalist" placeholder="Enter Your Mobile Number"><br>

            <label for="email">email Id</label><br>
            <input type="text" required="" pattern="[a-zA-Z0-9_.]*[@]{1}[a-zA-Z]*[.]{1}[a-zA-Z]{2,5}" value="" name="email" id="email" list="email_datalist" placeholder="Enter Your email Id"><br>

            <label for="dateOfBirth">Date Of Birth</label><br>
            <input type="text" required="" pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" value="" name="dateOfBirth" id="dateOfBirth" list="dateOfBirth_datalist" placeholder="YYYY-MM-DD"><br>
        <button type="submit" class="button">Next</button>
</div>

        <div id="address" style="display:none;">
            <label for="doorNo">Door No</label><br>
            <input type="text" required="" pattern="^[a-zA-Z0-9/- ]{1,20}$" value="" name="doorNo" id="doorNo" placeholder="Enter The Door Number "><br>

             <label for="street">Street Name</label><br>
             <input type="text"  value="" name="street" id="street" placeholder="Enter The Street Name "><br>

             <label for="area">Area</label><br>
             <input type="text"  value="" name="area" id="area" placeholder="Enter The Area Name "><br>

             <label for="state">State</label><br>
             <input type="text" required="" pattern="^[a-zA-Z ]{1,20}$"  value="" name="state" id="state" placeholder="Enter The State "><br>

             <label for="country">Country</label><br>
             <input type="text" required="" pattern="^[a-zA-Z ]{1,20}$"  value="" name="country" id="country" placeholder="Enter The Country Name "><br>

             <label for="pincode">Zip Code</label><br>
             <input type="text" required="" pattern="^[1-9]{1}[0-9]{5}$"  value="" name="pincode" id="pincode" placeholder="Enter The Country Name "><br>
             <button type="button" class="button">Prev</button>
             <input type="submit" value="Submit" >
        </div>

        </form>
    </div>
</body>
</html>

