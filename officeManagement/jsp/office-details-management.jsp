<html>
<body>
    <link href='http://localhost:8080/officeManagement/jsp/btn.css' rel='stylesheet' type='text/css'>
    <div class="container">
        <a href="http://localhost:8080/officeManagement/jsp/employee-details-management.jsp" class="btn">
            <svg width="278" height="60">
                <defs>
                    <linearGradient id="grad1">
                    <stop offset="0%" stop-color="#FF8282"/>
                    <stop offset="100%" stop-color="#E178ED" />
                    </linearGradient>
                </defs>
                <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
            </svg>
            <span>Employee Management</span>
        </a>

        <a href="http://www.google.com" class="btn">
            <svg width="278" height="60">
                <defs>
                    <linearGradient id="grad1">
                    <stop offset="0%" stop-color="#FF8282"/>
                    <stop offset="100%" stop-color="#E178ED" />
                    </linearGradient>
                </defs>
                <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
            </svg>
            <span>Project Management</span>
        </a>

        <a href="http://www.google.com" class="btn">
            <svg width="278" height="60">
                <defs>
                    <linearGradient id="grad1">
                    <stop offset="0%" stop-color="#FF8282"/>
                    <stop offset="100%" stop-color="#E178ED" />
                    </linearGradient>
                </defs>
                <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad1)" width="266" height="50"></rect>
            </svg>
            <span>Client Management</span>
        </a>
    </div>
</body>
</html>
