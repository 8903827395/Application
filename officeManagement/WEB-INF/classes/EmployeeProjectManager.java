import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ideas2it.employeeprojectmanagement.client.controller.ClientController;
import com.ideas2it.employeeprojectmanagement.employee.controller.EmployeeController;
import com.ideas2it.employeeprojectmanagement.employeeproject.controller.EmployeeProjectController;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.logger.LoggerWrapper;
import com.ideas2it.employeeprojectmanagement.project.controller.ProjectController;
/**
 * <p>
 * The EmployeeProjectManager is an application that maintains the details of 
 * employees, projects and clients.
 * This application manages details of employees, projects and their work 
 * details.
 * </p>
 *
 * @created 31 Aug 2017
 * @author  Kosalram
 */
public class EmployeeProjectManager extends HttpServlet {
    /**
     * <p>
     * This is the main method of the application. It has different controller
     * to manage informations of employee, project and client.
     * </p>
     * 
     * @param args unused.
     */
    public static void main(String[] args) {
        boolean continueToManage = Boolean.TRUE;
        int option;
        LoggerWrapper.configLog4j();
        ClientController clientController = new ClientController();
        EmployeeController employeeController = new EmployeeController();
        EmployeeProjectController employeeProjectController = 
                                               new EmployeeProjectController();
        ProjectController projectController = new ProjectController();
        Scanner scanner = new Scanner(System.in);     

        while (continueToManage) {
            try {
                System.out.println("\n"+"Press 0 -- Employee Controller"+ 
                                   "\n"+"1--Project Controller "+
                                   "\n"+"2--Assign Project for Employees"+
                                   "\n"+"3--DeAssign Project for Employees"+
                                   "\n"+"4--Client Controller"+
                                   "\n"+"5--Exit");
                option = scanner.nextInt();
                scanner.nextLine();
                switch (option) {
                    case 0:
                        employeeController.manageEmployee();
                        break;
                    case 1:
                        projectController.manageProject();
                        break;
                    case 2:
                        employeeProjectController.assignProject();
                        break;
                    case 3:
            //            employeeProjectController.deassignProject();
                        break;
                    case 4:
                        clientController.manageClient();
                        break;
                    case 5:
                        continueToManage = Boolean.FALSE;
                        break;
                    default :
                        System.out.println("Invalid Input Try Again");            
                }   
            } catch (InputMismatchException e) {
                System.out.println("You can enter only numerical values.");
                continueToManage = Boolean.FALSE;            
            }
        }
    }     
}        
