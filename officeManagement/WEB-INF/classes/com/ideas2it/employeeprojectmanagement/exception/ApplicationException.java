package com.ideas2it.employeeprojectmanagement.exception;

import java.lang.Exception;

/**
 * <p>
 * The ApplicationException class gets exception message or cause for the exception.
 * </p>
 */
public class ApplicationException extends Exception {
    
    public ApplicationException () {
    }

    public ApplicationException (String message) {
        super(message);
    }

    public ApplicationException (Throwable cause) {
        super(cause);
    }

    public ApplicationException (String message, Throwable cause) {
        super(message, cause);
    }
}
