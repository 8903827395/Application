package com.ideas2it.employeeprojectmanagement.logger;

import java.io.File;
import java.lang.Exception;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * <p>
 * The LoggerWrapper class is the generic logger class to print the exception 
 * message in an xml file.
 * </p>
 *
 * @author  kosalram
 */ 
public class LoggerWrapper {
 
    private static Logger logger;
 
    public static void configLog4j () {
        String log4jConfigFile ="/home/ubuntu/Desktop/hibernate/com/ideas2it/employeeprojectmanagement/logger/loggerConfig.xml";
        DOMConfigurator.configure(log4jConfigFile);
    }

    public static void createLogInstance(Class className) {
        logger = Logger.getLogger(className);
    }

    public static void error(String message, Exception e) {
        logger.error(message, e);
    }

    public static void info(String message) {
        logger.info(message);
    }

}

