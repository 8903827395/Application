package com.ideas2it.employeeprojectmanagement.employeeproject.controller;

import java.util.List;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService;
import com.ideas2it.employeeprojectmanagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.model.Project;
import com.ideas2it.employeeprojectmanagement.project.service.ProjectService;
import com.ideas2it.employeeprojectmanagement.project.service.impl.ProjectServiceImpl;

/**
 * <p>
 * The EmployeeProjectController class is primarily used for allocating and 
 * deallocating projects to the employees.
 * </p>
 *  
 * @created  01 Sep 2017
 * @author   Kosalram
 */
public class EmployeeProjectController {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();

    /**
     * <p>
     * The assignProject method gets the employee Id and project code from user
     * and checks the availability in corresponding tables, then it allocates 
     * employee to the project.
     * </p>
     */
    public void assignProject() {
        boolean doesEmployeeNotAssigned = Boolean.TRUE;
        int option;
        String id;
        String code;
        Scanner scanner = new Scanner(System.in);
        List<Employee> employees = null;

        try {
            System.out.println("Enter the Project Code :");
            code = scanner.next();
            if (projectService.checkCode(code)) {
                Project project = projectService.retrieveProjectByCode(code);
                employees = project.getEmployees();
                while (doesEmployeeNotAssigned) {
                    System.out.println("Press 0--Add employee to "+
                                       "project\n 1--Confirm Assigning"+
                                       " Employees\n 2--Exit");
                    option = scanner.nextInt();
                    switch (option) {
                        case 0:
                            System.out.println("Enter the Employee Id :");
                            id = scanner.next();
                            if (employeeService.checkId(id)) {
                                Employee employee = 
                                       employeeService.retrieveEmployeeById(id);
                                employees.add(employee);
                            } else {
                                System.out.println("Employee Id unavailable.");
                            }
                            break;
                        case 1:
                            if(employees != null) {
                                project.setEmployees(employees);
                                projectService.modifyProject(project);
                            } else {
                                System.out.println("You have not assigned any"+
                                                   " Employee");
                            }
                            break;
                        case 2:
                            doesEmployeeNotAssigned = Boolean.FALSE;
                            break;
                        default :
                            System.out.println("Invalid Entry");
                    }
                }
            } else {
                System.out.println("Project Code Not Available:");
            }                
        } catch (ApplicationException e) {
            System.out.println(e.getMessage());
        }
    }
}
