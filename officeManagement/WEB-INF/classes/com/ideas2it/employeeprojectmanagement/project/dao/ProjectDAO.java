package com.ideas2it.employeeprojectmanagement.project.dao;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.model.Project;

/**
 * <p>
 * The ProjectDAO interface contains the method declarations of ProjectDAOImpl 
 * class.
 * </p>
 * 
 * @created 31 Aug 2017
 * @author  Kosalram
 */
public interface ProjectDAO {
    
    /**
     * <p>
     * The insertProject method creates connection to the 
     * EmployeeProjectManagement database and executes query to insert the 
     * record in databse.
     * </p>  
     *
     * @param Project project 
     *        The project model contains information of project
     *
     * @return Project
     *        It returns the project model along with project Code 
     *
     * @throws ApplicationException when connecting to database or adding details in 
     *                         the database.
     */
    void insertProject(Project project) throws ApplicationException;

    /**
     * <p>
     * The getProject method establishes connection to the database
     * and retrieves information of active projects.
     * </p>
     * 
     * @return List<Project>
     *         It returns the list of projects
     *
     * @throws ApplicationException when connecting to database or retrieving details 
     *                         from the database.
     */
    List<Project> getProjects() throws ApplicationException;

    /**
     * <p>
     * This method gets the project model and delete from the database.
     * </p>
     *        
     * @param String code
     *        It refers the unique code of project
     *
     * @throws ApplicationException when connecting to database or deleting details 
     *                         from the database.
     */
    void deleteProject(String code) throws ApplicationException;

    /**
     * <p>
     * The update method gets the Project model with information that is to be 
     * updated.
     * </p>
     *
     * @param Project project
     *        The project model contains information of project.
     *
     * @throws ApplicationException when connecting to database or updating details 
     *                         in the database.
     */
    boolean updateProject(Project project) throws ApplicationException;
    
    /**
     * <p>
     * This method checks the project record with the help of project code
     * and returns the boolean value.
     * </p>
     *          
     * @param String code
     *        It refers the unique code of project
     *
     * @returns true if project  exists
     *          false if project does not exist.
     *
     * @throws ApplicationException when connecting to database or checking details 
     *                         in the database.
     */
    boolean isProjectExists(String code) throws ApplicationException;

    /**
     * <p>
     * This method gets the project from the database with the help of project 
     * code and returns the boolean value.
     * </p>
     *          
     * @param String code
     *        It refers the unique code of project
     *
     * @return Project
     *        It returns the project model along with project Code 
     *
     * @throws ApplicationException when connecting to database or retrieving details 
     *                         in the database.
     */
    Project getProjectByCode(String code) throws ApplicationException;
}


