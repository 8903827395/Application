package com.ideas2it.employeeprojectmanagement.project.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.logger.LoggerWrapper;
import com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO;
import com.ideas2it.employeeprojectmanagement.project.model.Project;
import com.ideas2it.employeeprojectmanagement.util.HibernateUtil;

/**
 * <p>
 * The ProjectDAOimpl class implements ProjectDAO interface and communicates
 * with the EmployeeProjectManagement database.
 * </p>
 * 
 * @created   01 Sep 2017
 * @author    Kosalram.
 */
public class ProjectDAOimpl implements ProjectDAO {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    static {
        LoggerWrapper.createLogInstance(ProjectDAOimpl.class);
    }    
    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method Project insertProject(Project)
     */
    public void insertProject(Project project) throws ApplicationException {
        int id;
        String projectCode;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.save(project);
            id = project.getId();
            projectCode = "PROJ"+id;
            project.setProjectCode(projectCode);
            session.update(project);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error occured while inserting Project "+
                               project.getProjectName(), e);
            throw new ApplicationException("Error occured.Please try again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method List<Project> getProjects()
     */
    public List<Project> getProjects() throws ApplicationException {
        List<Project> projects;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("isProjectExist", Boolean.TRUE));
            projects = criteria.list(); 
            transaction.commit();
            return projects;  
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while retrieving Projects ", e);
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method void deleteProject(Project)
     */
    public void deleteProject(String code) throws ApplicationException {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            String hblQuery = "Update Project set isProjectExist = :value where projectCode = :id";
            Query query = session.createQuery(hblQuery);
            query.setParameter("value", Boolean.FALSE);
            query.setParameter("id", code);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while deleting project of "+
                                                                        code, e);     
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method void update(Project)
     */
    public boolean updateProject(Project project) throws ApplicationException {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.update(project);
            transaction.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while updating project of id:"+
                                                       project.getProjectCode(), e);  
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method boolean isProjectExists(Project)
     */
    public boolean isProjectExists(String code) throws ApplicationException {
        boolean exists = Boolean.FALSE;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Query query = session.createQuery("SELECT COUNT(*) FROM Project"+
                                              " where projectCode = :id AND"+
                                              " isProjectExist = :value");
            query.setParameter("id", code);
            query.setParameter("value", Boolean.TRUE);
            exists = ((long)query.uniqueResult()) > 0;
            return exists;
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error occured while checking for"+
                               " project record "+ code, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO
     * #method Project getProjectByCode(String)
     */
    public Project getProjectByCode(String code) throws ApplicationException {
        Project project = null;   
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("projectCode", code));
            project = (Project)criteria.uniqueResult(); 
            transaction.commit();
            return project;
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while searching for project "+
                                                                      code, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
}


