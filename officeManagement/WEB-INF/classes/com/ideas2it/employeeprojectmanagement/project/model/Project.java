package com.ideas2it.employeeprojectmanagement.project.model;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;

/**
 * Creating getters and setters for all details of Project.
 */
public class Project {
    private boolean isProjectExist;
    private int id;
    private String projectName;
    private String projectCode;
    private String projectDomain;
    private String projectDescription;
    private Client client;
    private List<Employee> employees;

    public Project (){
    }
    
    public Project(String projectName, String projectDomain, 
                            String projectDescription, Client client,
                            boolean isProjectExist) {
        this.projectName  = projectName;
        this.projectDomain = projectDomain;
        this.projectDescription = projectDescription;   
        this.client = client;    
        this.isProjectExist = isProjectExist;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setIsProjectExist(boolean isProjectExist) {
        this.isProjectExist = isProjectExist;
    }

    public boolean getIsProjectExist() {
        return this.isProjectExist;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectCode() {
        return projectCode;
    }
    
    public void setProjectName(String projectName) {
        this.projectName = projectName; 
    }
    
    public String getProjectName() {
        return projectName;
    } 

    public void setProjectDomain(String projectDomain) {
        this.projectDomain = projectDomain;
    }

    public String getProjectDomain() {
        return projectDomain;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return this.employees;
    }
   
    public String toString() {
        return "Project Code :"+projectCode+"\n"+
               "Project Name :"+projectName+"\n"+
               "Project Domain:"+projectDomain+"\n"+
               "Project Description:"+projectDescription+"\n";  
    }    
} 
