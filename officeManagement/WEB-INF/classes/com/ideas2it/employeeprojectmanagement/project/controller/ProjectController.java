package com.ideas2it.employeeprojectmanagement.project.controller;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeprojectmanagement.client.service.ClientService;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.model.Project;
import com.ideas2it.employeeprojectmanagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeprojectmanagement.project.service.ProjectService;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * <p>
 * The Project Controller class interacts with user and performs project CRUD 
 * operations.
 * </p>
 * 
 * @created  31 Aug 2017
 * @author   Kosalram
 */  
public class ProjectController {  
    private ClientService clientService = new ClientServiceImpl();   
    private ProjectService projectService = new ProjectServiceImpl();   
    private Scanner scanner = new Scanner(System.in);
    private ValidationUtil validationUtil = new ValidationUtil();  

    /**
     * <p>
     * The manageProject performs operations as per user interests.
     * </p>
     */   
    public void manageProject() {
        boolean isProjectNotManaged = Boolean.TRUE;
        int option;
        String code;
        List<Project> projects = new ArrayList<Project>();

        while (isProjectNotManaged ) {  
           try {
               System.out.println("Press 0--Create new Project \n"+
                                  "1--Display all active projects\n"+
                                  "2--Delete Project\n"+
                                  "3--Edit Project Details\n"+
                                  "4--Exit:");
               option = scanner.nextInt();
               scanner.nextLine();
               switch(option) {
                   case 0:
                       createNewProject();
                       break;
                   case 1:
                       projects = projectService.retrieveProjects();
                       for (Project project : projects) {
                           System.out.println(project.toString());                    
                       }
                       break;
                   case 2:
                       System.out.println("Enter Project Code to delete"+
                                          " record:");
                       code = scanner.nextLine();
                       if (projectService.removeProjectByCode(code)) {
                           System.out.println(code+" Project Deleted"+
                                                   " Successfully.");
                       } else {
                           System.out.println("Project Code does not exist"+
                                              " Deletion UnSuccessful.");
                       }
                       break;
                   case 3:
                       System.out.println("Enter Project Code to update"+
                                          " record:");
                       code = scanner.next();
                       editProjectDetails(code);
                       break;
                   case 4:
                       isProjectNotManaged = Boolean.FALSE;
                       break;
                   default:
                       System.out.println("Invalid input Please Try Again");
               }
            } catch (InputMismatchException e) {
                System.out.println("You can enter only numerical values.");
                isProjectNotManaged = Boolean.FALSE;
            } catch (ApplicationException e) {
                System.out.println(e.getMessage());
                isProjectNotManaged = Boolean.FALSE;
            }
        }
    }
    
    /**
     * <p>
     * The editProjectDetails method updates project information based on user
     * interests with the help project Code.
     * </p>
     * 
     * @param String code
     *        This is the project Code which is unique for each projwct
     */   
    public void editProjectDetails(String code) {
       boolean doesProjectNotUpdated = Boolean.TRUE;
        int option;
        String projectName;
        String projectDomain;
        String projectDescription;

        try {
            if (projectService.checkCode(code)) {
                Project project = projectService.retrieveProjectByCode(code);
                displayProject(project);  
                while(doesProjectNotUpdated) {
                    System.out.println("Press 0--Change Project Name \n"+
                                       "1--Change project Domain\n"+
                                       "2--Change project Description\n"+
                                       "3--Confirm changes\n"+
                                       "4--Exit"); 
                    option = scanner.nextInt();
                    scanner.nextLine();
                    switch(option) {
                        case 0:
                            System.out.println("Enter Project Name:");
                            projectName = scanner.nextLine();
                            if ((validationUtil.validateName(projectName))) {
                                project.setProjectName(projectName);
                            } else {
                                System.out.println("Enter Proper Name");
                            }
                            break;
                        case 1:
                            System.out.println("Enter Project Domain:");
                            projectDomain = scanner.next();
                            if ((validationUtil.validateProjectDomain(projectDomain))) {
                                project.setProjectDomain(projectDomain);
                            } else {
                                System.out.println("Enter valid Contact"+
                                                   "Number");                                    
                            }
                            break;
                        case 2:
                            System.out.println("Enter Project description:");
                            projectDescription = scanner.next();
                            project.setProjectDescription(projectDescription);                            
                            break;
                        case 3:
                            System.out.println(" Press 1--> Submit Changes"+
                                               " or Any key to Edit Again");
                            option = scanner.nextInt();
                            scanner.nextLine();
                            if(option == 1) {
                                projectService.modifyProject(project);
                                displayProject(project);    
                            }
                            else {
                                doesProjectNotUpdated = Boolean.TRUE;
                            }
                            break;
                        case 4:
                            doesProjectNotUpdated = Boolean.FALSE;       
                            break;
                        default:
                            System.out.println("Invalid Input Try Again");
                    }
                }
            } else {
                System.out.println("Project Code unavailable.");
            }
        } catch (ApplicationException e) {
            System.out.println(e.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("You can enter only numerical values.");
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * The createNewProject method gets project information and stores in the 
     * database. 
     * </p>
     */
    public void createNewProject() throws ApplicationException {
        boolean isProjectExist = Boolean.TRUE;
        String clientId;
        String projectName; 
        String projectDomain;
        String projectDescription;

        System.out.println("Enter Client Id:");
        clientId = scanner.nextLine();
        Client client = clientService.retrieveClientById(clientId);
        if(clientService.checkId(clientId)) { 
            System.out.println("Enter Project Name:");
            projectName = scanner.nextLine();
            System.out.println("Enter Project Domain:");
            projectDomain = scanner.nextLine();
            System.out.println("Enter Project Description:");
            projectDescription = scanner.nextLine();
            if (isProjectDetailsValid(projectDomain, projectName)) {
                projectService.addProject(projectDomain, projectName, 
                                          projectDescription, client, 
                                          isProjectExist);
            } else {
                System.out.println("Please enter proper details");   
            }
        } else {
            System.out.println("Client Id invalid.");   
        }
    }
    
    /**
     * <p>
     * The isProjectDetailsValid gets project details and uses validation class
     * to validate the information.
     * </p>
     *
     * @param String projectDomain
     *        It is the domain name
     * @param String projectName 
     *        It refers the name of the project
     * 
     * @return true if all the information is valid
     *         false if any one information is invalid.
     */
    public boolean isProjectDetailsValid(String projectDomain, 
                                                          String projectName) {
        if (!(validationUtil.validateName(projectName))) {
            System.out.println("Please Enter Proper Name ");
        }
        else if (!(validationUtil.validateProjectDomain(projectDomain))) {
            System.out.println("Please Enter Valid Domain ");
        }        
        else {
            return Boolean.TRUE;    
        }
        return Boolean.FALSE;   
    }

    public void displayProject(Project project) {
        System.out.println(project.toString());                    
    }
}
