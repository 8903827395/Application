package com.ideas2it.employeeprojectmanagement.project.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.dao.impl.ProjectDAOimpl;
import com.ideas2it.employeeprojectmanagement.project.dao.ProjectDAO;
import com.ideas2it.employeeprojectmanagement.project.model.Project;
import com.ideas2it.employeeprojectmanagement.project.service.ProjectService;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * <p>
 * The ProjectServiceimpl class implements ProjectService interface.
 * </p>
 * 
 * @created  01 Sep 2017
 * @author   Kosalram
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDAO projectDao = new ProjectDAOimpl();
  
    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method Project addProject(String, String, String)
     */    
    public void addProject (String projectDomain, String projectName, 
                              String projectDescription, Client client,
                              boolean isProjectExist)
                              throws ApplicationException {
        Project project = new Project(projectDomain, projectName,
                                      projectDescription, client, isProjectExist); 
        projectDao.insertProject(project);
    }
      
    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method List<Project> retrieveProjects()
     */ 
    public List<Project> retrieveProjects() throws ApplicationException {
        List<Project> projects = new ArrayList<Project>();
        projects = projectDao.getProjects();  
        return projects;  
    }
    
    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method boolean removeProjectByCode(String)
     */ 
    public boolean removeProjectByCode(String code) throws ApplicationException {
       if(projectDao.isProjectExists(code)) {
            projectDao.deleteProject(code);
            return Boolean.TRUE;
        }
       return Boolean.FALSE;
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method boolean modifyProjectName(String, String)
     */ 
    public boolean modifyProject(Project project) throws ApplicationException {
        if (projectDao.updateProject(project)) {
            return Boolean.TRUE;
        } 
        return Boolean.FALSE;
    }
 
    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method boolean checkCode(String)
     */
    public boolean checkCode(String code) throws ApplicationException {
        return projectDao.isProjectExists(code);    
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.project.service.ProjectService
     * #method Project retrieveProjectByCode(String)
     */
    public Project retrieveProjectByCode(String code) throws ApplicationException {
        Project project = projectDao.getProjectByCode(code);
        if (project != null) {
            return project;
        }
        return null;
    }
}
