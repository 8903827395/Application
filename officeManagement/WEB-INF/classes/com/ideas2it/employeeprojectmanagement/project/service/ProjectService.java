package com.ideas2it.employeeprojectmanagement.project.service;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.model.Project;

/**
 * <p>
 * The ProjectService interface contains method declarations of Project Service.
 * </p>
 * 
 * @created    01 Sep 2017
 * @author  Kosalram
 */
public interface ProjectService {
    
    /**
     * <p>
     * The addProject method gets the details like project name,project
     * domain and project description. 
     * It sets the values in project model and send it to projectDao class.
     * </p>
     * 
     * @param String projectDomain
     *        It contains the domain of the project. 
     * @param String projectName
     *        The name of the project.
     * @param String projectDescription
     *        It has the details of the project
     * @return Project
     *        The project model is returned
     * @throws ApplicationException when connecting to database or adding details in 
     *                         the database.
     */
    void addProject(String projectDomain, String projectName, 
                       String projectDescription, Client client,
                       boolean isProjectExist)
                       throws ApplicationException; 
      
    /**
     * <p>
     * The retrieveProjects method shows the entire list of project details
     * from the database.
     * </p>
     *
     * @return List<Project>
     *        The list of projects is returned.
     * @throws ApplicationException when connecting to database or retrieving details  
     *                         from the database.
     */
    List<Project> retrieveProjects() throws ApplicationException;

     /**
     * <p>
     * The removeProjectByCode method gets the project model and sets the projectCode  
     * in the model and sends it to the project DAO class.
     * </p>
     *
     * @param String code
     *        It refers the unique code of project
     * 
     * @return true if project is deleted successfully
     *         false if project deletion unsuccessful.  
     *  
     * @throws ApplicationException when connecting to database or removing details  
     *                         from the database.   
     */
    boolean removeProjectByCode(String code) throws ApplicationException;
    
    /**
     * <p> 
     * The modifyProjectName method gets project object from the controller.
     * The complete model is sent to Dao layer after validation for updating 
     * details in database.
     * </p>
     * 
     * @param String projectName
     *        It refers project name.
     * @param String code
     *        It refers the unique code of project
     * 
     * @return true if project name is updated successfully
     *         false if project name updation fails.   
     *
     * @throws ApplicationException when connecting to database or retrieving details  
     *                         from the database.    
     */
    boolean modifyProject(Project project) throws ApplicationException;

  
    /**
     * <p> 
     * The checkCode method gets  project object from the controller.
     * The complete model is sent to Dao layer to look for the project record.
     * </p>
     * 
     * @param String code
     *        It refers the unique code of project
     * 
     * @return true if project exists 
     *         false if project does not exists.       
     *
     * @throws ApplicationException when connecting to database or checking details  
     *                         from the database
     */
    boolean checkCode(String code) throws ApplicationException ;

    /**
     * <p> 
     * The retrieveProjectByCode method gets project object from the controller.
     * The complete model is sent to Dao layer to look for the project record
     * </p>
     * 
     * @param String code
     *        It refers the unique code of project
     * 
     * @return project model.  
     *
     * @throws ApplicationException when connecting to database or retrieving details  
     *                         from the database     
     */ 
   Project retrieveProjectByCode(String code) throws ApplicationException ;
}
