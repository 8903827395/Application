package com.ideas2it.employeeprojectmanagement.employee.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;  

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService;
import com.ideas2it.employeeprojectmanagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.util.DateUtil;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * <p>
 * The Employee Controller class interacts with user and performs employee CRUD 
 * operations.
 * </p>
 * 
 * @created  31 Aug 2017
 * @author   Kosalram
 */ 
public class EmployeeController { 
    private AddressService addressService = new AddressServiceImpl();
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private Scanner scanner = new Scanner(System.in); 
    private ValidationUtil validationUtil = new ValidationUtil();

    /**
     * <p>
     * The manageEmployee method performs CRUD operations as per user interests.
     * </p>
     */
    public void manageEmployee() {    
        boolean isEmployeeNotManaged = Boolean.TRUE;
        int option;
        String id;  
        List<Employee> employees;
        List<Address> addresses = new ArrayList<Address>();

        while (isEmployeeNotManaged) {  
           try {
               System.out.println("Press 0--Create New Employee\n"+
                                  "1--Display All Active Employee\n"+ 
                                  "2--Delete Employee \n"+ 
                                  "3--Edit Employee Details\n"+
                                  "4--Exit :");
               option = scanner.nextInt();
               scanner.nextLine();
               switch (option) {
                   case 0:
                       createNewEmployee();
                       break;
                   case 1:
                       employees = employeeService.retrieveEmployees();
                       for (Employee employee : employees) {
                           addresses = employee.getAddresses();
                           System.out.println(employee.toString());                    
                           for (Address address : addresses) {
                               System.out.println(address.toString());
                           }
                       }
                       break;
                   case 2:
                       System.out.println("Enter Employee ID to delete"+
                                          " record:");
                       id = scanner.next();
                       if (employeeService.removeEmployeeById(id)) {
                           System.out.println(id+" Employee Deleted"+
                                                 " Successfully.");
                       } else {
                           System.out.println("EmployeeId does not exist."+
                                              "Deletion UnSuccessful.");
                       }
                       break;
                   case 3:
                       System.out.println("Enter Employee ID to update"+
                                          " record:");
                       id = scanner.next();
                       editEmployeeDetails(id);
                       break;
                   case 4:
                       isEmployeeNotManaged = Boolean.FALSE;
                       break;
                   default:
                       System.out.println("Invalid input Please Try Again");
               }
           } catch (InputMismatchException e) {
               System.out.println("You can enter only numerical values.");
               isEmployeeNotManaged = Boolean.FALSE;
           } catch (ApplicationException e) {
               System.out.println(e.getMessage());
               isEmployeeNotManaged = Boolean.FALSE;
           }
        }
    }
 
    /**
     * <p>
     * The editEmployeeDetails method gets employee Id as argument and updates as
     * per user choice.
     * @param String id
     *        This refers the employee Id . It checks the availability in the
     *        EmployeeProjectManagement database and does appropriate updations
     *        as requested by the User.
     * </p>
     * 
     * <p>
     * The Id format should contain a prefix 'ITI' followed by a number.
     * Eg. ITI1, ITI12 and so on.
     * </p>
     */
    public void editEmployeeDetails(String id) {
        boolean doesEmployeeNotUpdated = Boolean.TRUE;
        int option;
        String dateOfBirth;
        String email;
        String mobile;
        String name;
     
        try {
            if (employeeService.checkId(id)) {
                Employee employee = employeeService.retrieveEmployeeById(id);
                displayEmployee(employee);    
                while (doesEmployeeNotUpdated) {
                    System.out.println("Press 0--Change Name \n"+
                                       "1--Change Mobile Number\n"+
                                       "2--Change eMail Id\n"+
                                       "3--Change Date Of Birth\n"+
                                       "4--Confirm changes\n"+
                                       "5--Exit"); 
                    option = scanner.nextInt();
                    scanner.nextLine();
                    switch(option) {
                        case 0:
                            System.out.println("Enter Employee Name:");
                            name = scanner.nextLine();
                            if ((validationUtil.validateName(name))) {
                                employee.setName(name);
                            } else {
                                System.out.println("Enter Proper Name");
                            }
                            break;
                        case 1:
                            System.out.println("Enter Mobile Number:");
                            mobile = scanner.next();
                            if ((validationUtil.validateMobileNumber(mobile))) {
                                employee.setMobile(mobile);                            
                            } else {
                                System.out.println("Enter valid Mobile"+
                                                   "Number");                                    
                            }
                            break;
                        case 2:
                            System.out.println("Enter E mail Id:");
                            email = scanner.next();
                            if ((validationUtil.validateEmail(email))) {
                                employee.setEmail(email);                            
                            } else {
                                System.out.println("Enter valid Email Id");                                    
                            }
                            break;
                        case 3:
                            System.out.println("Enter date Of Birth:");
                            dateOfBirth = scanner.next();
                            if ((DateUtil.validateDate(dateOfBirth))) {
                                employee.setDateOfBirth(dateOfBirth);                            
                            } else {
                                System.out.println("Invalid Date Of Birth");                                    
                            }
                            break;
                        case 4:
                            System.out.println(" Press 1--> Submit Changes"+
                                               " or Any key to Edit Again");
                            option = scanner.nextInt();
                            scanner.nextLine();
                            if(option == 1) {
                                employeeService.modifyEmployee(employee);
                                displayEmployee(employee);    
                            }
                            else {
                                doesEmployeeNotUpdated = Boolean.TRUE;
                            }
                            break;
                        case 5:
                            doesEmployeeNotUpdated = Boolean.FALSE;       
                            break;
                        default:
                            System.out.println("Invalid Input Try Again");
                    }
                }
            } else {
                System.out.println("Employee ID unavailable.");
            }
        } catch (ApplicationException e) {
            System.out.println(e.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("You can enter only numerical values.");
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * The createNewEmployee method gets personal information of employee and 
     * stores in the database.
     * Then it calls the getEmployeeAddress method and fetch the user address. 
     * </p>
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while inserting into database.
     */
    public void createNewEmployee() throws ApplicationException {
        char availability = 'Y';
        String dateOfBirth;
        String email;
        String mobile;
        String name;
        List<Address> addresses;

        System.out.println("Enter Employee Name:");
        name = scanner.nextLine();
        System.out.println("Enter Mobile Number:");
        mobile = scanner.next();
        System.out.println("Enter Email ID:");
        email = scanner.next();
        scanner.nextLine();
        System.out.println("Enter Date Of Birth in YYYY-MM-DD :");
        dateOfBirth = scanner.nextLine();
        if (isEmployeeDetailsValid(email, mobile, name, dateOfBirth)) {
            if (isAgeValid(dateOfBirth)) {
                System.out.println("Enter Employee Address:");
                addresses = getEmployeeAddress();
                employeeService.addEmployee(addresses,email, mobile, name, 
                                                    dateOfBirth, availability);  
            } else {
	        System.out.println("Invalid Date of Birth.Employee"+ 
                                   "should have completed atleast 18"+
                                   "years of age.");
            }     
        } else {
            System.out.println("Please enter proper details");
        }
    } 

    /**
     * <p>
     * The isAgeValid method gets dateOfBirth in string format. It uses date 
     * util and calculate age.
     * </p>
     *
     * @param String dateOfBirth
     *        the date of birth of an employee.
     * @return true if the age equals or greater than 18
     *         false if the age is below 18
     */
    public boolean isAgeValid(String dateOfBirth) {
        DateUtil dateUtil = new DateUtil();
        Calendar dobDate = dateUtil.convertStringToDate(dateOfBirth);
        int age = dateUtil.calculateDateDifference(dobDate);
        if (age >= 18) {
            return Boolean.TRUE;   
        }
        return Boolean.FALSE;
    }

    /**
     * <p>
     * The getEmployeeAddress method gets address of employee.This
     * method then adds the employee address to the database.
     * </p>
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while inserting into database.
     */
    public List<Address> getEmployeeAddress() throws ApplicationException {
        char continueToAdd = 'Y';
        int pincode;
        String area;
        String country;
        String doorNo;
        String state;
        String street;
        List<Address> addresses = new ArrayList<Address>();

        do {
            System.out.println("Enter Door Number:");
            doorNo = scanner.nextLine();
            System.out.println("Enter Street Name:");
            street = scanner.nextLine();
            System.out.println("Enter Area/Locality:");
            area = scanner.nextLine();
            System.out.println("Enter State :");
            state = scanner.nextLine();
            System.out.println("Enter Country :");
            country = scanner.nextLine();
            System.out.println("Enter Pincode:");
            pincode = scanner.nextInt(); 
            Address address = addressService.addAddress(doorNo, street,
                                                        area, state, country,
                                                        pincode);
            addresses.add(address);
            System.out.println("Do you want to add another address: press 'Y' or 'y' ");
            continueToAdd = scanner.next().charAt(0);
            scanner.nextLine();
        } while(continueToAdd == 'Y' || continueToAdd == 'y');
        return addresses;
    }

    /**
     * <p>
     * The isEmployeeDetailsValid gets personel information of employee 
     * and validates it.
     * </p>
     *
     * @return true if all the employee information is valid
     *         false if any one information is invalid.
     */
    public boolean isEmployeeDetailsValid(String email, String mobile, 
                                             String name, String dateOfBirth) {
        DateUtil dateUtil = new DateUtil();

        if (!(validationUtil.validateName(name))) {
            System.out.println("Please Enter Proper Name ");
        } else if (!(validationUtil.validateMobileNumber(mobile))) {
            System.out.println("Please Enter Proper Mobile ");
        } else if (!(validationUtil.validateEmail(email))) {
            System.out.println("Please Enter Proper email Id ");
        } else if (!(dateUtil.validateDate(dateOfBirth))) {
            System.out.println("Invalid Date Format");
        } else {
            return Boolean.TRUE;    
        }
        return Boolean.FALSE;    
    }

    public void displayEmployee(Employee employee) {
        List<Address> addresses = employee.getAddresses();
        System.out.println(employee.toString());                    
        for (Address address : addresses) {
            System.out.println(address.toString());
        }
    }
} 
