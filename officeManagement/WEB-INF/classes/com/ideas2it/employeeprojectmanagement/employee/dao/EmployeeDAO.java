package com.ideas2it.employeeprojectmanagement.employee.dao;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

/**
 * <p>
 * The EmployeeDAO interface gives access to specific methods of implementing 
 * classes.
 * </p>
 * 
 * @created 31 Aug 2017
 * @author Kosalram
 */
public interface EmployeeDAO {
        
    /**
     * <p>
     * The insertEmployee method creates connection to the EmployeeProjectManagement
     * database and executes queries to perform required operations.
     * </p>
     * 
     * @param Employee employee
     *        Each data from the model is extracted and it is sent to the 
     *        database.It does not contain address and employeeId initially.
     * @return Employee 
     *        The complete Employee model along with employeeId is 
     *        returned.
     *        
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while inserting into database.
     */
    void insertEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * The getEmployees method establishes connection to the database
     * and retrieves information of active employees in a list.
     * </p>
     * 
     * @return List<Employee> 
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while getting employees from database.
     */
    List<Employee> getEmployees() throws ApplicationException;

    /**
     * <p>
     * This method gets the employeeId from user and soft deletes from the 
     * database.
     * </p>
     * 
     * @param String id
     *        The employee Id is obtained from user and the necessary record is 
     *        found with help of employee Id.
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while deleting from database.
     */
    void deleteEmployee(String id) throws ApplicationException; 

    /**
     * <p>
     * The updateEmployee method gets the Employee model with information that is to be 
     * updated.
     * </p>
     *
     * @param Employee employee
     *        The employee model contains information of employee.
     * 
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while updating in database.
     */
    boolean updateEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * This method checks the employee record with the help of employeeId
     * and returns the boolean value.
     * </p>
     * 
     * @param String id
     *        It refers the employee Id
     *
     * @returns true if employee Id exists in the database.
     *          false if employee Id does not exist.
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while checking for employee in database.
     */
    boolean isEmployeeExists(String id) throws ApplicationException;

    /**
     * <p>
     * The getEmployeeById method gets the employee Id as input and gives the
     * corresponding employee details.
     * </p>
     *
     * @param String id
     *        The employee Id is obtained from user and the necessary record is 
     *        found with help of employee Id.
     * @return Employee 
     *        The complete Employee model is returned.
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while getting Employee from database.
     */
    Employee getEmployeeById(String id) throws ApplicationException; 
}

