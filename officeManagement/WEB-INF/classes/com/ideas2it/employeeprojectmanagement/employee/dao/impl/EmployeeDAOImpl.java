package com.ideas2it.employeeprojectmanagement.employee.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Filter;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.logger.LoggerWrapper;
import com.ideas2it.employeeprojectmanagement.util.HibernateUtil;

/**
 * <p>
 * The EmployeeDAOImpl class implements EmployeeDAO interface and communicates
 * with the EmployeeProjectManagement database.
 * </p>
 *
 * @created 01 Sep 2017
 * @author  Kosalram.
 */
public class EmployeeDAOImpl implements EmployeeDAO {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    static {
        LoggerWrapper.createLogInstance(EmployeeDAOImpl.class);
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method Employee insertEmployee(Employee)
     */
    public void insertEmployee (Employee employee) throws ApplicationException {
        int id;
        String employeeId;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.save(employee);
            id = employee.getId();
            employeeId = "ITI"+id;
            employee.setEmployeeId(employeeId);
            session.update(employee);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error occured while inserting Employee "+
                                                        employee.getName(), e);
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
    
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method List<Employee> getEmployees()
     */
    public List<Employee> getEmployees() throws ApplicationException {
        List<Employee> employees;
        List<Address> addresses;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Filter filter = session.enableFilter("employeeFilter");
            filter.setParameter("value", 'Y');
            Query query = session.createQuery("from Employee e ");
            employees = query.list();
            transaction.commit();
            return employees;    
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while retrieving Employees ", e);
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
 
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method Employee getEmployeeById(String)
     */
    public Employee getEmployeeById(String id)throws ApplicationException {
        Employee employee = null;   
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            String hblQuery = "From Employee where employeeID = :id";
            Query query = session.createQuery(hblQuery);
            query.setParameter("id", id);
            employee = (Employee)query.list().get(0);
            transaction.commit();
            return employee;
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while searching for employee "+
                                                                         id, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method void deleteEmployee(String)
     */
    public void deleteEmployee(String id)throws ApplicationException {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            String hblQuery = "Update Employee set availability = :value where employeeID = :id";
            Query query = session.createQuery(hblQuery);
            query.setParameter("value", "N");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while deleting employee of "+
                                                                         id, e);     
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
 
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method void updateEmployee(Employee)
     */
    public boolean updateEmployee(Employee employee) throws ApplicationException{
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.update(employee);
            transaction.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while updating employee of id:"+
                                                          employee.getId(), e);  
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO
     * #method boolean isEmployeeExists(String)
     */
    public boolean isEmployeeExists(String id) throws ApplicationException {
        boolean exists = Boolean.FALSE;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Query query = session.createQuery("SELECT COUNT(*) FROM Employee"+
                                              " where employeeID = :id AND"+
                                              " availability = :value");
            query.setParameter("id", id);
            query.setParameter("value", "Y");
            exists = ((long)query.uniqueResult()) > 0;
            return exists;
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while checking for"+
                               " employee record "+ id, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
}


