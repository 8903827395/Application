package com.ideas2it.employeeprojectmanagement.employee.model;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.project.model.Project;

/**
 * Creating getters and setters for all details of Employee.
 */
public class Employee {
    private char availability;
    private int id;
    private String dateOfBirth;
    private String name;
    private String mobile;
    private String email;
    private String employeeId;
    private List<Address> addresses;
    private List<Project> projects;

    public Employee() {
    }

    public Employee(List<Address> addresses, String email, String mobile, String name, 
                    String dateOfBirth, char availability) {
        this.addresses = addresses;
        this.email  = email;       
        this.mobile = mobile;
        this.name   = name;       
        this.dateOfBirth = dateOfBirth;
        this.availability = availability;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return this.employeeId;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;  
    }

    public String getMobile() {
        return mobile;
    }

    public void setName(String name) {
        this.name = name; 
    }
    
    public String getName() {
        return name;
    } 

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth; 
    }
    
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setAvailability(char availability) {
        this.availability = availability; 
    }
    
    public char getAvailability() {
        return availability;
    } 

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Project> getProjects() {
        return this.projects;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public String toString() {
        return "Employee ID :"+employeeId+"\n"+"Employee Name :"+name+"\n"+
               "Mobile Number :"+mobile+"\n"+"Employee email Id:"+email+"\n"+
               "DateOfBirth:"+dateOfBirth+"\n";  
    }
} 
