package com.ideas2it.employeeprojectmanagement.employee.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeprojectmanagement.employee.dao.EmployeeDAO;
import com.ideas2it.employeeprojectmanagement.employee.dao.impl.EmployeeDAOImpl;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * <p>
 * The EmployeeServiceimpl method implements EmployeeService interface.It 
 * provides services like adding,removing,retrieving and modifying Employee
 * details.
 * </p>
 *
 * @created  01 Sep 2017
 * @author   Kosalram
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDAO employeeDao = new EmployeeDAOImpl();
    private AddressService addressService = new AddressServiceImpl();
           
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method String addEmployee(String, String, String, String)
     */
    public void addEmployee(List<Address> addresses, String email, String mobile,
                            String name, String dob, char availability)
                            throws ApplicationException {
        Employee employee = new Employee(addresses, email, mobile, name, dob,
                                         availability); 
        employeeDao.insertEmployee(employee); 
    }  

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method List<Employee> retrieveEmployees()
     */
    public List<Employee> retrieveEmployees() throws ApplicationException {
        AddressService addressService = new AddressServiceImpl();
        List<Employee> employees;
        employees = employeeDao.getEmployees();  
        return employees;   
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method boolean removeEmployeeById(String)
     */
    public boolean removeEmployeeById(String id) throws ApplicationException {
        if (employeeDao.isEmployeeExists(id)) {
            employeeDao.deleteEmployee(id);
            return Boolean.TRUE;
        }     
        return Boolean.FALSE;
    }
    
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method boolean modifyEmployeeName(String, String)
     */
    public boolean modifyEmployee(Employee employee) throws ApplicationException {
        if (employeeDao.updateEmployee(employee)) {
            return Boolean.TRUE;
        } 
        return Boolean.FALSE;
    }

  
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method boolean checkId(String)
     */
    public boolean checkId(String id) throws ApplicationException {
        return employeeDao.isEmployeeExists(id);    
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService
     * #method Employee retrieveEmployeeById(String)
     */
    public Employee retrieveEmployeeById(String id) throws ApplicationException {
        Employee employee = employeeDao.getEmployeeById(id);
        if (employee != null) {
            return employee;
        }
        return null;
    }
}
