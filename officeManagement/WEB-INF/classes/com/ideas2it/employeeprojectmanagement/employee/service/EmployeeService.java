package com.ideas2it.employeeprojectmanagement.employee.service;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

/**
 * <p>
 * The EmployeeService interface provides support to manage the employee 
 * details in the database.
 * This class utilises Employee DAO class to record the information in database.
 * It also uses Validation Util class to validate the user information.
 * </p>

 * @created  17 Aug 2017
 * @author   Kosalram
 */
public interface EmployeeService {

    /**
     * <p>
     * The addEmployee method get the user information and sets in Employee 
     * model.
     * Then the details are passed to the EmployeeDao class.
     * </p>
     * 
     * @param String email
     *        The validated email Id is obtained from user.
     * @param String mobile
     *        The validated mobile number is obtained from user.It should be 
     *        10 - digit number.The first number should start with digit 7 or
     *        8 or 9 and the remaining digits vary from 0 through 9.
     *        Example:9500032654.
     * @param String name
     *        The validated name is obtained from user.It should be either 
     *        lowercase or uppercase letters and can include spaces.Periods not 
     *        allowed.
     * @param String dob
     *        The validated dateOfBirth is obtained from user.
     *
     * @return String 
     *         After successfully adding the employee details this method 
     *         returns the Employee id. 
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while inserting into database.
     */
    void addEmployee(List<Address>addresses, String email, String mobile, String name, 
                     String dob, char availability) throws ApplicationException;

    /**
     * <p>
     * The retrieveEmployees method shows the entire list of active employee
     * details from the database.
     * </p>
     * 
     * @return List<Employee> 
     *         It retrieves the list of employees from the database
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while retrieving into database.
     */
    List<Employee> retrieveEmployees() throws ApplicationException;

    /**
     * <p>
     * The removeEmployeeById method gets the employee model and sets the employee id  
     * in the model and sends it to the employee DAO class where the record is
     * deleted.
     * </p>
     *
     * @param String id
     *        It refers the employee Id
     * 
     * @return true if employee is deleted successfully
     *         false if employee deletion unsuccessful.  
     * 
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while removing employee from database.     
     */
    boolean removeEmployeeById(String id) throws ApplicationException;

    /**
     * <p> 
     * The nodifyEmployeeName method gets employee object from the controller.
     * The complete model is sent to Dao layer after validation for updating 
     * details in database.
     * </p>
     * 
     * @param String name
     *        It is the employee name
     *
     * @param String id
     *        It refers the employee Id
     * 
     * @return true if employee name is updated successfully
     *         false if employee name updation fails.    
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while modifying in database.   
     */
    boolean modifyEmployee(Employee employee) throws ApplicationException;

  
    /**
     * <p> 
     * The checkId method gets employee object from the controller.
     * The complete model is sent to Dao layer to look for the employee record
     * </p>
     * 
     * @param String id
     *        It refers the employee Id
     * 
     * @return true if employee exists 
     *         false if employee does not exists. 
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while checking the details in database.      
     */
    boolean checkId(String id) throws ApplicationException;
    
    /**
     * <p> 
     * The retrieveEmployeeById method gets employee object from the controller.
     * The complete model is sent to Dao layer to look for the employee record
     * </p>
     * 
     * @param String id
     *        It refers the employee Id.
     * 
     * @return employee
     *        It returns employee model.      
     *
     * @throws ApplicationException if problem occurs while connecting to database or
     *                         while retrieving an employee from database.
     */
    Employee retrieveEmployeeById(String id) throws ApplicationException;
}

