package com.ideas2it.employeeprojectmanagement.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
    try {
        Configuration configuration = new Configuration();
        System.out.println("Hibernate Configuration loaded");
        SessionFactory sessionFactory = configuration.configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
        return sessionFactory;
    } catch (Throwable ex) {
        System.err.println("Initial SessionFactory creation failed." + ex);
        ex.printStackTrace();
        throw new ExceptionInInitializerError(ex);
    }
}

public static SessionFactory getSessionFactory() {
    if(sessionFactory == null) 
        sessionFactory = buildSessionFactory();
    return sessionFactory;
    }
}

