package com.ideas2it.employeeprojectmanagement.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * The Validation Util class gets the user input and checks for the 
 * appropriate pattern.
 * </p>
 *
 * @created  01 Sep 2017
 * @author   Kosalram
 */
public class ValidationUtil {

    /**
     * <p>
     * The validateName method gets name and checks the pattern 
     * provided.It may be either lowercase or uppercase letters.It also includes
     * spaces. Periods not allowed.
     * </p>
     * 
     * @param String name
     *        It gets the name and check for the pattern provided.
     * @return true if name matches the pattern
     *         false if name does not match the pattern
     */
    public boolean validateName(String name) {
        return Pattern.matches("[a-zA-Z ]+", name);
    }

    /**
     * <p>
     * This method gets mobile number from user and validates it.The first 
     * number should start with digit 7 or 8 or 9 and the remaining digits
     * vary from 0 through 9.Example:9500032654.
     * </p>
     * 
     * @param String mobile
     *        It gets the user mobile number. 
     * @return  true if mobile number matches the pattern
     *         false if mobile number does not match the pattern
     */
    public boolean validateMobileNumber(String mobile) {
        return Pattern.matches("[789]{1}\\d{9}", mobile); 
    }

    /**
     * <p>
     * The validateEmail method gets employee email and checks the pattern 
     * provided.
     * Eg: ksoal.13ece@konug.edu
     * </p>
     *  
     * @param  String email
     *         It gets the email provided by user.
     * @return true if email matches the pattern
     *        false if email does not match the pattern
     */
    public boolean validateEmail(String email) {
        return Pattern.matches("[a-zA-Z0-9_.]*"+"[@]{1}"+"[a-zA-Z]*"+"[.]{1}"+
                            "[a-zA-Z]{2,5}", email); 
    }
      
    /**
     * <p>
     * The validateProjectDomain method gets project domain and checks the  
     * pattern provided.It may be either lowercase or uppercase letters.It also includes
     * spaces. Periods not allowed.
     * </p>
     * 
     * @param  String domain
     *         It gets the project domain from user.
     * @return true if Project Domain matches the pattern
     *         false if Project Domain does not match the pattern
     */
    public boolean validateProjectDomain(String domain) {
            return Pattern.matches("[a-zA-Z ]+", domain); 
    }
 
}

