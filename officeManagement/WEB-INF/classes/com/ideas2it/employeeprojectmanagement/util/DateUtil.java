package com.ideas2it.employeeprojectmanagement.util;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * The DateUtil classes validate the date formats.
 * </p>
 *
 * @created  01 Sep 2017
 * @author    Kosalram
 */

public class DateUtil {
    
    /**
     * <p>
     * The validateDate method gets the date in YYYY-MM-DD format and checks 
     * with the pattern provided.
     * </p>
     * 
     * @param  String date.
     *         It gets date in string format.
     * @return true if date matches the pattern
     *         false if date does not match the pattern
     */
    public static boolean validateDate(String date) {
        return Pattern.matches("((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]"+
                               "|[12][0-9]|3[01])", date); 
    }

    /**
     * <p>
     * The calculate age method gets date in string format and parses to date
     * format.
     * </p>
     *
     * @param String date.
     *        It gets date in string format.
     * @return dob 
     *        dob in date format
     */
    public static Calendar convertStringToDate(String date) {
        Calendar dob = Calendar.getInstance();
        if(date == null)
            return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dob.setTime(sdf.parse(date));
        }
        catch (ParseException e) {
            System.out.println(e);
        }
        return dob;
    }

    /**
     * <p>
     * This method gets date of birth and calculates the age and returns it.
     * </p>
     *
     * @param Calendar dob
     *        The date is used to calculate the age.
     * @return age
     *        After calculation  an integer value is returned as age.
     */
    public static int calculateDateDifference(Calendar dob) {
        int age;
        int birthDay;
        int birthMonth;
        int birthYear;
        int currentDay;
        int currentMonth;
        int currentYear;
 
        Calendar today = Calendar.getInstance();
        currentYear = today.get(Calendar.YEAR);
        birthYear = dob.get(Calendar.YEAR);
        age = currentYear - birthYear;
        currentMonth = today.get(Calendar.MONTH);
        birthMonth = dob.get(Calendar.MONTH);
        if (birthMonth > currentMonth) { 
            age--;
        } 
        else if (birthMonth == currentMonth) { 
            currentDay = today.get(Calendar.DAY_OF_MONTH);
            birthDay = dob.get(Calendar.DAY_OF_MONTH);
            if (birthDay > currentDay) { 
                age--;
            }
        }
        return age;
    }
       
}

