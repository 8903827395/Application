package com.ideas2it.employeeprojectmanagement.client.controller;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;    
import java.util.Scanner;  

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.client.service.ClientService;
import com.ideas2it.employeeprojectmanagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.project.service.ProjectService;
import com.ideas2it.employeeprojectmanagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * <p>
 * The Client Controller class interacts with user and performs client CRUD 
 * operations.
 * </p>
 * 
 * @created  31 Aug 2017
 * @author   Kosalram
 */
public class ClientController {  
    private AddressService addressService = new AddressServiceImpl();
    private ClientService clientService = new ClientServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    private Scanner scanner = new Scanner(System.in);
    private ValidationUtil validationUtil = new ValidationUtil();     

    /**
     * <p>
     * The manageClient performs CRUD operations as per user interests.
     * </p>
     */
    public void manageClient() {    
        boolean isClientNotManaged = Boolean.TRUE;
        int option;
        String id;
        List<Address> addresses = new ArrayList<Address>();
        List<Client> clients = new ArrayList<Client>();

        while (isClientNotManaged) { 
            try { 
                System.out.println("Press 0--Create new Client\n"+
                                   "1--Display all active Clients\n"+ 
                                   "2--Delete Client \n"+ 
                                   "3--Edit Client Details\n"+
                                   "4--Exit :");
                option = scanner.nextInt();
                scanner.nextLine();
                switch(option) {
                    case 0:
                        createNewClient();
                        break;
                    case 1:
                        clients = clientService.retrieveClients();
                        for (Client client : clients) {
                            addresses = client.getAddresses();
                            System.out.println(client.toString());                    
                            for (Address address : addresses) {
                                System.out.println(address.toString());
                            }
                        }
                        break;
                    case 2:
                        System.out.println("Enter Client ID to delete record:");
                        id = scanner.next();
                        if (clientService.removeClientById(id)) {
                            System.out.println(id+"Client Deleted Successfully.");
                        } else {
                            System.out.println("ClientId does not exist"+
                                               "Deletion UnSuccessful.");
                        }
                        break;
                    case 3:
                        System.out.println("Enter Client ID to update record:");
                        id = scanner.next();
                        editClientDetails(id);
                        break;
                    case 4:
                        isClientNotManaged = Boolean.FALSE;
                        break;
                    default:
                        System.out.println("Invalid input Please Try Again");
                } 
            } catch (InputMismatchException e) {
                System.out.println(e.getMessage());
            } catch (ApplicationException e) {
                System.out.println("Error.Try again");
                isClientNotManaged = Boolean.FALSE;
            } 
        }
    }
 
    /**
     * <p>
     * The editClientDetails method gets client Id as argument and updates as
     * per user choice.
     * @param String id
     *        This refers the client Id . It checks the availability in the
     *        EmployeeProjectManagement database and does appropriate updations
     *        as requested by the User.
     * </p>
     * 
     * <p>
     * The Id format should be COMP_+number.
     * Eg. COMP_1, COMP_12 and so on.
     * </p>
     */
    public void editClientDetails(String id) throws ApplicationException {
        boolean doesClientNotUpdated = Boolean.TRUE;
        int option;
        String contact;
        String email;
        String name;

        try {
            if (clientService.checkId(id)) {
                Client client = clientService.retrieveClientById(id);
                displayClient(client);  
                while(doesClientNotUpdated) {
                    System.out.println("Press 0--Change Name \n"+
                                       "1--Change Contact Number\n"+
                                       "2--Change eMail Id\n"+
                                       "3--Confirm changes\n"+
                                       "4--Exit"); 
                    option = scanner.nextInt();
                    scanner.nextLine();
                    switch(option) {
                        case 0:
                            System.out.println("Enter Client Name:");
                            name = scanner.nextLine();
                            if ((validationUtil.validateName(name))) {
                                client.setName(name);
                            } else {
                                System.out.println("Enter Proper Name");
                            }
                            break;
                        case 1:
                            System.out.println("Enter Contact Number:");
                            contact = scanner.next();
                            if ((validationUtil.validateMobileNumber(contact))) {
                                client.setContact(contact);                            
                            } else {
                                System.out.println("Enter valid Contact"+
                                                   "Number");                                    
                            }
                            break;
                        case 2:
                            System.out.println("Enter E mail Id:");
                            email = scanner.next();
                            if ((validationUtil.validateEmail(email))) {
                                client.setEmail(email);                            
                            } else {
                                System.out.println("Enter valid Email Id");                                    
                            }
                            break;
                        case 3:
                            System.out.println(" Press 1--> Submit Changes"+
                                               " or Any key to Edit Again");
                            option = scanner.nextInt();
                            scanner.nextLine();
                            if(option == 1) {
                                clientService.modifyClient(client);
                                displayClient(client);    
                            }
                            else {
                                doesClientNotUpdated = Boolean.TRUE;
                            }
                            break;
                        case 4:
                            doesClientNotUpdated = Boolean.FALSE;       
                            break;
                        default:
                            System.out.println("Invalid Input Try Again");
                    }
                }
            } else {
                System.out.println("Client ID unavailable.");
            }
        } catch (ApplicationException e) {
            System.out.println(e.getMessage());
        } catch (InputMismatchException e) {
            System.out.println("You can enter only numerical values.");
            e.printStackTrace();
        }
    }
    
    /**
     * <p>
     * The createNewClient method gets the client information. 
     * </p>
     */
    public void createNewClient() throws ApplicationException {
        boolean isClientExist = Boolean.TRUE;
        String contact;
        String email;
        String id;
        String name;
        List<Address> addresses;

        System.out.println("Enter Client Name:");
        name = scanner.nextLine();
        System.out.println("Enter Contact:");
        contact = scanner.next();
        System.out.println("Enter Email ID:");
        email = scanner.next();
        scanner.nextLine();
        if (isClientDetailsValid(email, contact, name)) {
            System.out.println("Enter Client Address:");
            addresses = getClientAddress();     
            clientService.addClient(addresses, email, contact, name, isClientExist);    
        } else {
            System.out.println("Client addition failed");
        }
    }
 
    /**
     * <p>
     * The getClientAddress method gets address of the client and 
     * stores in the database.
     * </p>
     */
    public List<Address> getClientAddress() throws ApplicationException {
        char continueToAdd = 'Y';
        int pincode;
        String area;
        String country;
        String doorNo;
        String state;
        String street;
        List<Address> addresses = new ArrayList<Address>();

        do {
            System.out.println("Enter Door Number:");
            doorNo = scanner.nextLine();
            System.out.println("Enter Street Name:");
            street = scanner.nextLine();
            System.out.println("Enter Area/Locality:");
            area = scanner.nextLine();
            System.out.println("Enter State :");
            state = scanner.nextLine();
            System.out.println("Enter Country :");
            country = scanner.nextLine();
            System.out.println("Enter Pincode:");
            pincode = scanner.nextInt(); 
            Address address = addressService.addAddress(doorNo, street,
                                                        area, state, country,
                                                        pincode);
            addresses.add(address);
            System.out.println("Do you want to add another address: press 'Y' or 'y' ");
            continueToAdd = scanner.next().charAt(0);
            scanner.nextLine();
        } while(continueToAdd == 'Y' || continueToAdd == 'y');
        return addresses;
    }

    /**
     * <p>
     * The isClientDetailsValid gets client information and validates it.
     * </p>
     *
     * @return true if all the information is valid
     *         false if any one information is invalid.
     */
    public boolean isClientDetailsValid(String email, String contact, 
                                                                 String name) {
        if (!(validationUtil.validateName(name))) {
            System.out.println("Please Enter Proper Name ");
        } else if (!(validationUtil.validateMobileNumber(contact))) {
            System.out.println("Please Enter Proper Mobile ");
        } else if (!(validationUtil.validateEmail(email))) {
            System.out.println("Please Enter Proper email Id ");
        } else {
            return Boolean.TRUE;    
        }
        return Boolean.FALSE;    
    }

    public void displayClient(Client client) {
        List<Address> addresses = client.getAddresses();
        System.out.println(client.toString());                    
        for (Address address : addresses) {
            System.out.println(address.toString());
        }
    }
}
