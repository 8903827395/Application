package com.ideas2it.employeeprojectmanagement.client.model;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.project.model.Project;

/**
 * Creating getters and setters for all details of client.
 */
public class Client {
    private boolean isClientExist;
    private int id;
    private String name;
    private String contact;
    private String email;
    private String clientId;

    private List<Address> addresses;
    private List<Project> projects;
 
    public Client() {
    }

    public Client(List<Address> addresses, String email, String contact,
                                         String name, boolean isClientExist) {
        this.addresses = addresses;
        this.email = email;       
        this.contact = contact;
        this.name = name;   
        this.isClientExist = isClientExist;    
    }

    public void setIsClientExist(boolean isClientExist) {
        this.isClientExist = isClientExist;
    }

    public boolean getIsClientExist() {
        return this.isClientExist;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setContact(String contact) {
        this.contact = contact;  
    }

    public String getContact() {
        return contact;
    }

    public void setName(String name) {
        this.name = name; 
    }
    
    public String getName() {
        return name;
    } 

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return this.clientId;
    }
 
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }
 
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Project> getProjects() {
        return this.projects;
    }

    public String toString() {
        return "Client Id:"+clientId+"\n"+"Company name:"+name+"\n"+
               "Contact :"+contact+"\n"+"email :"+email+"\n";  
    }  
}    
