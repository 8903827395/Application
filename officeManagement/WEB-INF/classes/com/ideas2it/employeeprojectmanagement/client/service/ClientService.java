package com.ideas2it.employeeprojectmanagement.client.service;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

/**
 * <p>
 * The ClientService interface provides support to manage the clients 
 * details in the database.
 * This class utilises Client DAO class to manage the information in database.
 * </p>
 * 
 * @created  17 Aug 2017
 * @author   Kosalram
 */
public interface ClientService {
    
    /**
     * <p>
     * The addClient method get the user information and sets in Client model.
     * Then the details are passed to the clientDao class.
     * </p>
     * 
     * @param String email
     *        The validated email Id is obtained from user.
     * @param String contact
     *        The validated contact number is obtained from user.It should be 
     *        10 - digit number.The first number should start with digit 7 or
     *        8 or 9 and the remaining digits vary from 0 through 9.
     *        Example:9500032654.
     * @param String name
     *        The validated name is obtained from user.It should be either 
     *        lowercase or uppercase letters and can include spaces.Periods not 
     *        allowed.
     * @return String
     *         After successfully adding the client details this method 
     *         returns the CLient id. 
     * 
     * @throws ApplicationException when connecting to database or when adding client
     *                         information.
     */
    void addClient(List<Address> addresses, String email, String contact,
                   String name, boolean isClientExist) 
                   throws ApplicationException;

    /**
     * <p>
     * The retrieveClients method shows the entire list of clients and their 
     * details from the database.
     * </p>
     *
     * @return List<Client>
     *         It returns the list of clients.
     *
     * @throws ApplicationException when connecting to database or when retrieving 
     *                         client information.
     */
    List<Client> retrieveClients() throws ApplicationException;

    /**
     * <p>
     * The removeClientById method gets the client id and sends it to the 
     * client DAO class where the record is deleted.
     * </p>
     *
     * @param String id
     *        It refers the client id.
     * 
     * @return true if client is deleted successfully
     *         false if client deletion unsuccessful.     
     *
     * @throws ApplicationException when connecting to database or when removing 
     *                         client information.  
     */
    boolean removeClientById(String id) throws ApplicationException;

    /**
     * <p> 
     * The modifyClientName method gets client name and client id.
     * It is sent to Dao layer after validation for updating 
     * details in database.
     * </p>
     * 
     * @param String name
     *        It refers the name of the client.
     * @param String id
     *        It refers the client id.
     * 
     * @return true if client name is updated successfully
     *         false if client name updation fails.  
     *     
     * @throws ApplicationException when connecting to database or when modifying 
     *                         client information.
     */
    boolean modifyClient(Client client) throws ApplicationException;

    /**
     * <p> 
     * The checkId method gets client id from the controller.
     * It sends the id to Dao for the checking the existence of Client.
     * </p>
     * 
     * @param String id
     *        It refers the client id.
     * 
     * @return true if client exists 
     *         false if client does not exists.       
     *     
     * @throws ApplicationException when connecting to database or when checking 
     *                         client in database
     */
    boolean checkId(String id) throws ApplicationException;

    /**
     * <p> 
     * The retrieveClientById method gets client id from the controller.
     * It is sent to Dao layer to look for the client record and gets the client
     * model if present.
     * </p>
     * 
     * @return Client client
     *        The client model holds records of a client
     * 
     * @throws ApplicationException when connecting to database or when retrieving 
     *                         client in database      
     */
    Client retrieveClientById(String id) throws ApplicationException;
}

