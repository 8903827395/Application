package com.ideas2it.employeeprojectmanagement.client.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeprojectmanagement.client.dao.ClientDAO;
import com.ideas2it.employeeprojectmanagement.client.dao.impl.ClientDaoImpl;
import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.client.service.ClientService;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.util.ValidationUtil;

/**
 * The ClientServiceimpl method implements ClientService interface.
 * @date     01 Sep 2017
 * @author   Kosalram
 */
public class ClientServiceImpl implements ClientService {
    private ClientDAO clientDao = new ClientDaoImpl();
       
    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method String addClient(String, String, String)
     */
    public void addClient(List<Address> addresses, String email, String contact,
                   String name, boolean isClientExist) 
                   throws ApplicationException {
        Client client = new Client(addresses, email, contact, name, isClientExist); 
        clientDao.insertClient(client);
    }  

    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method List<Client> retrieveClients()
     */
    public List<Client> retrieveClients() throws ApplicationException {
        AddressService addressService = new AddressServiceImpl();
        List<Client> clients = clientDao.getClients();  
        return clients; 
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method boolean removeClientById(String)
     */
    public boolean removeClientById(String id) throws ApplicationException {
        if(clientDao.isClientExists(id)) {
            clientDao.deleteClient(id);
            return Boolean.TRUE;
        }     
        return Boolean.FALSE;
    }
    
    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method boolean modifyClientName(String, String)
     */
    public boolean modifyClient(Client client) throws ApplicationException {
        if (clientDao.updateClient(client)) {
            return Boolean.TRUE;
        } 
        return Boolean.FALSE;
    } 

    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method boolean checkId(String)
     */
    public boolean checkId(String id) throws ApplicationException {
        return clientDao.isClientExists(id);    
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.client.service.ClientService
     * #method Client retrieveClientById(String)
     */
    public Client retrieveClientById(String id) throws ApplicationException {
        Client client = clientDao.getClientById(id);
        if (client != null) {
            return client;
        }
        return null;
    }
}
