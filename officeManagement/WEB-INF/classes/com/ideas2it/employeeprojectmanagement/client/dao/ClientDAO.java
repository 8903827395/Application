package com.ideas2it.employeeprojectmanagement.client.dao;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

/**
 * <p>
 * The ClientDAO interface interacts with Client database using queries.
 * </p>
 *
 * @created 31 Aug 2017
 * @author  Kosalram
 */
public interface ClientDAO {
        
    /**
     * <p>
     * The insertClient method creates connection to the Client database and
     * executes queries to perform insert operations.
     * </p>
     * 
     * @param Client client
     *        It contains client model with new details to add in database.
     * 
     * @return Client
     *        It returns the client model with client ID
     * @throws ApplicationException when connecting to database or when inserting 
     *                         client in database      
     */
    void insertClient(Client client) throws ApplicationException;

    /**
     * <p>
     * The getClients method establishes connection to the database
     * and retrieves information of active clients.
     * </p>
     * 
     * @return List<Client>
     *         The list of clients are returned.
     * @throws ApplicationException when connecting to database or when getting 
     *                         clients from database      
     */
    List<Client> getClients() throws ApplicationException;

    /**
     * <p>
     * This method gets the clientId from user and delete from the database.
     * </p>
     * 
     * @param String id
     *        It refers the client id
     * @throws ApplicationException when connecting to database or when deleting 
     *                         client from database      
     */
    void deleteClient(String id) throws ApplicationException; 

    /**
     * <p>
     * The updateClient method gets the Client object and updates in the database.
     * </p>
     *  
     * @param Client client
     *        The client model contains the details of client whose details 
     *        require updation. 
     * @throws ApplicationException when connecting to database or when updating 
     *                         client in database      
     */
    boolean updateClient(Client client) throws ApplicationException;

    /**
     * <p>
     * This method checks the client record with the help of clientId
     * and returns the boolean value.
     * </p>
     *
     * @param String id
     *        The client Id is obtained from user and the necessary record is 
     *        found with help of client Id.
     * @returns true if client Id exists
     *          false if client Id does not exist.
     * @throws ApplicationException when connecting to database or when checking 
     *                         client in database 
     */
    boolean isClientExists(String id) throws ApplicationException;
    
    /**
     * <p>
     * The getClientById method gets the client Id as input and gives the
     * corresponding client details.
     * </p>
     *
     * @param String id
     *        The client Id is obtained from user and the necessary record is 
     *        found with help of client Id.
     * @return Client 
     *        The complete Client model is returned.
     * @throws ApplicationException when connecting to database or when getting 
     *                         client from database 
     */
    Client getClientById(String id) throws ApplicationException;
}

