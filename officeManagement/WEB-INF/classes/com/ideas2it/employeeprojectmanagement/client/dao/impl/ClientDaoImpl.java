package com.ideas2it.employeeprojectmanagement.client.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.employeeprojectmanagement.client.dao.ClientDAO;
import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.logger.LoggerWrapper;
import com.ideas2it.employeeprojectmanagement.util.HibernateUtil;

/**
 * The ClientDAOimpl class implements ClientDAO interface and communicates
 * with the ClientProjectManagement database.
 * @date    01 Sep 2017
 * @author  Kosalram.
 */
public class ClientDaoImpl implements ClientDAO {
    static {
        LoggerWrapper.createLogInstance(ClientDaoImpl.class);
    }
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method Client insertClient(Client)
     */
    public void insertClient(Client client) throws ApplicationException {
        int id;
        String clientId;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.save(client);
            id = client.getId();
            clientId = "COMP_"+id;
            client.setClientId(clientId);
            session.update(client);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error occured while inserting Client "+
                                             client.getName(), e);
            throw new ApplicationException("Error Try Again");
        } finally {
            session.close();
        }
    }
    
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method List<Client> getClients()
     */
    public List<Client> getClients() throws ApplicationException {
        List<Client> clients;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Criteria criteria = session.createCriteria(Client.class);
            criteria.add(Restrictions.eq("isClientExist", Boolean.TRUE));
            clients = criteria.list(); 
            transaction.commit();
            return clients; 
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while retrieving clients ", e);
            throw new ApplicationException("Error Try Again.");
        }
        finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method Client getClientById(String)
     */
    public Client getClientById(String id) throws ApplicationException {
        Client client = null;   
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Criteria criteria = session.createCriteria(Client.class);
            criteria.add(Restrictions.eq("clientId", id));
            client = (Client)criteria.uniqueResult(); 
            transaction.commit();
            return client;
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while searching for client "+
                                                                        id, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method void deleteClientById(Client)
     */
    public void deleteClient(String id) throws ApplicationException {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            String hblQuery = "Update Client set isClientExist = :value where clientId = :id";
            Query query = session.createQuery(hblQuery);
            query.setParameter("value", Boolean.FALSE);
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while deleting client of "+
                                                                        id, e);     
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
 
    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method void updateClient(Client)
     */
    public boolean updateClient(Client client) throws ApplicationException{
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.update(client);
            transaction.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            transaction.rollback();
            LoggerWrapper.error("Error Occured while updating client of id:"+
                                                       client.getClientId(), e);  
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.employee.dao.ClientDAO
     * #method boolean isClientExists(Client)
     */
    public boolean isClientExists(String id) throws ApplicationException {
        boolean exists = Boolean.FALSE;
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            Query query = session.createQuery("SELECT COUNT(*) FROM Client"+
                                              " where clientId = :id AND"+
                                              " isClientExist = :value");
            query.setParameter("id", id);
            query.setParameter("value", Boolean.TRUE);
            exists = ((long)query.uniqueResult()) > 0;
            return exists;
        } catch (HibernateException e) {
            LoggerWrapper.error("Error occured while checking for"+
                               " client record "+ id, e);           
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
}


