package com.ideas2it.employeeprojectmanagement.address.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.employeeprojectmanagement.address.dao.AddressDao;
import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;
import com.ideas2it.employeeprojectmanagement.logger.LoggerWrapper;
import com.ideas2it.employeeprojectmanagement.util.HibernateUtil;

public class AddressDaoImpl implements AddressDao {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    static {
        LoggerWrapper.createLogInstance(AddressDaoImpl.class);
    }

    /**
     * @see com.ideas2it.employeeprojectmanagement.address.dao.AddressDao
     * #method void addAddress(String, String)
     */
    public void insertAddress(Address address) throws ApplicationException {
        int id;
        Client client = address.getClient();
        Employee employee = address.getEmployee();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.save(address);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            LoggerWrapper.error("Error occured while inserting address", e);
            throw new ApplicationException("Error Try Again.");
        } finally {
            session.close();
        }
    }
}
