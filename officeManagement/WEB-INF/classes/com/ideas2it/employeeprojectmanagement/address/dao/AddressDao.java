package com.ideas2it.employeeprojectmanagement.address.dao;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

public interface AddressDao {
    
    /**
     * <p>
     * This method insert Address to the Address table.
     * </p>
     * 
     * @param Address address 
     *        It is the address model which holds either the client address or 
     *        employee address.
     *
     * @throws ApplicationException when connecting to database or while adding
     *                         address information.
     */
    void insertAddress(Address address) throws ApplicationException;
}
