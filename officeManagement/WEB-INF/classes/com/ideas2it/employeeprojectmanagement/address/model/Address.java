package com.ideas2it.employeeprojectmanagement.address.model;

import com.ideas2it.employeeprojectmanagement.client.model.Client;
import com.ideas2it.employeeprojectmanagement.employee.model.Employee;

/**
 * <p>
 * This model class contains getters and setters of Address.
 * </p>
 */
public class Address {
    private int id;
    private int pincode;
    private int address_holder;   
    private String area;
    private String country;
    private String door_no;
    private String state;
    private String street; 

    private Employee employee = null;
    private Client client = null;

    public Address() {
    }

    public Address(String door_no, String street, String area, String state, 
                                                 String country, int pincode) {
        this.door_no = door_no;       
        this.street = street;
        this.area = area;       
        this.state = state;
        this.country = country;
        this.pincode = pincode;
    }

    public void setAddress_holder(int address_holder) {
        this.address_holder = address_holder;
    }
    
    public int getAddress_holder() {
        return this.address_holder;
    }
 
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    } 

    public void setDoor_no(String door_no) {
        this.door_no = door_no;
    }
    
    public String getDoor_no() {
        return this.door_no;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    
    public String getStreet() {
        return this.street;
    }

    public void setArea(String area) {
        this.area = area;
    }
    
    public String getArea() {
        return this.area;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public String getState() {
        return this.state;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getCountry() {
        return this.country;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    
    public int getPincode() {
        return this.pincode;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public String toString() {
        return "Address :"+door_no+","+street+",\n"+area+",\n"+
                state+",\n"+country+"-"+pincode+"\n";  
    }    
}
