package com.ideas2it.employeeprojectmanagement.address.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.dao.AddressDao;
import com.ideas2it.employeeprojectmanagement.address.dao.impl.AddressDaoImpl;
import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.address.service.AddressService;
import com.ideas2it.employeeprojectmanagement.client.service.ClientService;
import com.ideas2it.employeeprojectmanagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeprojectmanagement.employee.service.EmployeeService;
import com.ideas2it.employeeprojectmanagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;

public class AddressServiceImpl implements AddressService {
    private AddressDao addressDao = new AddressDaoImpl();

    /**
     * @see com.ideas2it.employeeprojectmanagement.address.service
     * #void addAddress(String, String, String, String, String, String, int)
     */
    public Address addAddress(String doorNo, String street, 
                           String area, String state, String country,
                           int pincode) {
        Address address = new Address(doorNo, street, area, state, country,
                                                                      pincode);
        return address; 
    }
}
