package com.ideas2it.employeeprojectmanagement.address.service;

import java.util.List;

import com.ideas2it.employeeprojectmanagement.address.model.Address;
import com.ideas2it.employeeprojectmanagement.exception.ApplicationException;


public interface AddressService {
    
    /**
     * <p>
     * The addAddress method gets address details as arguments in addition to 
     * that it also gets either the employee Id or client Id.
     * </p>
     *                       
     * @param String id
     *        It refers either the employee Id or Client Id.
     * @param String door_no
     *        It accepts alphanumerics and the following characters['.','/','-']
     * @param String street
     *        It accepts lowercase and uppercase letters. It also includes
     *        spaces.
     * @param String area
     *        It accepts lowercase and uppercase letters. It also includes
     *        spaces.
     * @param String state
     *        It accepts lowercase and uppercase letters. It also includes
     *        spaces.
     * @param String country
     *        It accepts lowercase and uppercase letters. It also includes
     *        spaces.
     * @param int pincode
     *        It accepts numerical values and it should be 6 digits and should 
     *        not start with zero.
     * @throws ApplicationException when connecting to database or while adding
     *                         information.
     */
    Address addAddress(String door_no, String street, 
                           String area, String state, String country,
                           int pincode) throws ApplicationException;
}
