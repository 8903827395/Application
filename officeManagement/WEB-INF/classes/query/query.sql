create table Address(
id int not null auto_increment, 
door_no varchar(255),
street varchar(255),
area varchar(255), 
state varchar(255),
country varchar(255),
pincode int, 
primary key(id));

CREATE TABLE Employee(
id int(10) NOT NULL AUTO_INCREMENT,
employeeID varchar(255),
employeeName varchar(255),
mobileNumber varchar(10),
email varchar(255),
dateOfBirth varchar(10),
availability char(1),
PRIMARY KEY (id));

CREATE TABLE Project(
id int(10) NOT NULL AUTO_INCREMENT,
projectCode varchar(255),
projectName varchar(255),
projectDescription varchar(255),
projectDomain varchar(255),
isProjectExist boolean default true,
PRIMARY KEY (id));

CREATE TABLE Client(
id int(10) NOT NULL AUTO_INCREMENT,
clientId varchar(255),
companyName varchar(255),
contact varchar(10),
email varchar(255),
isClientExist boolean default true,
PRIMARY KEY (id));

CREATE TABLE Employee_Project(
id int not null auto_increment,
Employee_Id int,
Project_Code int,
primary key(id));


